#include <cmath>
#include <iostream>
#include <map>
#include <random>
#include <vector>

int main() {

  struct pop {
    enum class state { susceptible, infectious, recovered, dead };

    state sir = state::susceptible;
    size_t recover = 0;
  };

  // Create population and infect the first
  std::vector<pop> population(1000000);
  population.front().sir = pop::state::infectious;

  std::cout << "susceptible,infectious,recovered,dead,new infections\n";

  std::mt19937_64 rng;
  std::uniform_real_distribution<double> unif(0, 1);

  // Evolve population
  for (size_t i = 0; i < 50; ++i) {

    // Create summary
    std::map<pop::state, size_t> summary;
    for (const pop &p : population)
      ++summary[p.sir];

    // Current state
    const size_t susceptible = summary[pop::state::susceptible];
    const size_t infectious = summary[pop::state::infectious];
    const size_t recovered = summary[pop::state::recovered];
    const size_t dead = summary[pop::state::dead];

    // Change in infections
    const double r = 1.4;
    const size_t new_infections = std::lround(infectious * r);

    std::cout << susceptible << "," << infectious << "," << recovered << ","
              << dead << "," << new_infections << ",\n";

    // ranges::count_if

    // Review infected
    for (pop &p : population)
      if (p.sir == pop::state::infectious)
        if (p.recover > i) {

          const double currentRandomNumber = 100.0 * unif(rng);

          if (currentRandomNumber < 10)
            p.sir = pop::state::dead;
          else
            p.sir = pop::state::recovered;
        }

    // Infect the population accordingly
    size_t infected{0};
    for (auto &p : population)

      // New infections
      if (infected < new_infections)
        if (p.sir == pop::state::susceptible) {
          p.sir = pop::state::infectious;
          p.recover = i + 4;
          ++infected;
        }
  }
}
