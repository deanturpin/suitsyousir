all: sir.o

CXXFLAGS_SIMPLE := --std=c++20 --all-warnings --extra-warnings

# gcc flags
GCCCXXFLAGS := $(CXXFLAGS_SIMPLE) \
	-Werror -Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wattribute-alias -Wformat-overflow -Wformat-truncation \
	-Wmissing-attributes -Wstringop-truncation \
	-Wdeprecated-copy -Wclass-conversion \
	-Og
	# Add compat warning

CLANGCXXFLAGS := $(CXXFLAGS_SIMPLE) \
	-Werror -Wshadow -Wfloat-equal -Weffc++ -Wdelete-non-virtual-dtor \
	-Warray-bounds -Wdeprecated-copy

CXXFLAGS := $(GCCCXXFLAGS)

sir.o: sir.cpp
	g++-10 $(CXXFLAGS) -o $@ $<
	./$@

